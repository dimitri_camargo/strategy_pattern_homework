/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionactivity;

/**
 *
 * @author Dimitri
 */
public class ExceptionByZero {
    private int n1;
    private int n2;

    public ExceptionByZero() {
        this.n1 = 0;
        this.n2 = 0;
    }
    
    public int div(int n1, int n2){
        int res;
        if (n2 != 0) {
            res = n1/n2;
        } else {
            throw new ArithmeticException("Arithmetic Exception: there is a arithmetic process error");
        }
        return res;
    }
    
    
    
}
