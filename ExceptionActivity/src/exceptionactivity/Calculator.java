/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionactivity;

/**
 *
 * @author giovandemarco
 */
public class Calculator {

    protected double Number1;
    protected double Number2;

    //Class Construtor
    public Calculator() {
        //Default Values
        Number1 = 0.0;
        Number2 = 0.0;
    }

    //Overloading Constructor
    public Calculator(double Number1, double Number2) {
        this.Number1 = Number1;
        this.Number2 = Number2;
    }

    //Methods Overloading
    public double Sum() {

        return this.Number1 + this.Number2;
    }

    public static double Sum(double Number1, double Number2) {

        return Number1 + Number2;
    }
    
    public double Sub(){
        return this.Number1 - this.Number2;
    }
    
    public static double Sub(double Number1, double Number2){
        return Number1 - Number2;
    }
    
    public double Mult(){
        return this.Number1*this.Number2;
    }
    
    public static double  Mult(double Number1, double Number2){
        return Number1*Number2;
    }
    
    public double Div(){
        return this.Number1/this.Number2;
    }
    public static double Div(double Number1, double Number2){
        return Number1/Number2;
    }
      public String getAllResultsCalc(){
        return "Sum:"+this.Sum()+"\n"+
                "Sub:"+this.Sub()+"\n"+
                "Mult:"+this.Mult()+"\n"+
                "Div:"+this.Div();
    }
    
    

}//end-class
