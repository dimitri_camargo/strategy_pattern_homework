/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionactivity;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Dimitri
 */
public class ExceptionActivity {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            //int res = 4/0;
            //String s = "s";
//            int sInt = Integer.parseInt("s");//s
            
            //Reading Object
            Scanner read = new Scanner(System.in);
            double Number1, Number2;
            System.out.print("Enter the First Number:");
            Number1 = read.nextDouble();
            System.out.print("Enter the Second Number:");
            Number2 = read.nextDouble();
            //The Calculator Object Null
            Calculator obj = new Calculator();
            obj = new Calculator(Number1, Number2);
            
            //I N T (EXCEPTION BY ZERO)
            System.out.print("Enter the First int Number:");
            int intNumber1 = read.nextInt();
            System.out.print("Enter the Second int Number:");
            int intNumber2 = read.nextInt();
            //The Calculator Object Null
            ExceptionByZero obj2 = new ExceptionByZero();
            //obj2 = new ExceptionByZero(intNumber1, intNumber2);
            System.out.println("int division result: "+obj2.div(intNumber1, intNumber2));
            
            //Printing All Informations
            System.out.println(obj.getAllResultsCalc());
        }catch (InputMismatchException e1) {
            System.out.println("wrong numbers, please try again with integer numbers.");
            System.out.println(e1.getLocalizedMessage());
            System.out.println(Arrays.toString(e1.getStackTrace()));
        }catch(NullPointerException e2){
            System.out.println("there is a null pointer exception");
            System.out.println(e2.toString());                
        }catch (NumberFormatException e3) {
            System.out.println("there is a wrong parse of String to a number format");
            System.out.println(e3.getMessage());
            System.out.println(Arrays.toString(e3.getStackTrace()));
        }catch (Exception e) {//a genneric option
            System.out.println("sorry there is an Exception(Error), Try again");
        }
                
/*2) Ainda na Calculadora implemente uma classe chamada
ExcecaoPorZero (ArithmeticException) que será disparada
pela calculadora (throw) toda vez que se tentar uma divisão
por zero.*/
    }
    
}
